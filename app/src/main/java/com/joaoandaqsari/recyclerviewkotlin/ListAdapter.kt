package com.joaoandaqsari.recyclerviewkotlin


import android.content.ClipData
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.list_layout.view.*

/**
 * Created by aq on 12/13/2017.
 */
class ListAdapter (var c:Context , var lists :ArrayList<String>, var listsNum :ArrayList<String>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder? {
        var v = LayoutInflater.from(c).inflate(R.layout.list_layout, parent, false)
        return Item(v)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as Item).bindData(lists[position], listsNum[position])
    }

    class Item(itemView :View): RecyclerView.ViewHolder(itemView){
        fun bindData(_list : String, _num : String){
            itemView.numView.text =  _num
            itemView.textView.text = _list
        }
    }
}