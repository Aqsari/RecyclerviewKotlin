package com.joaoandaqsari.recyclerviewkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.ListAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        list.layoutManager = LinearLayoutManager(this)
        list.hasFixedSize()
        list.adapter =  ListAdapter(this, getLists(),getListsNum())
    }

    private fun getLists(): ArrayList<String> {
        var lists = ArrayList<String>()
        lists.add("java")
        lists.add("kotlin")
        lists.add("php")
        lists.add("swift")
        lists.add("java script")
        lists.add("mysql")
        return lists;
    }

    private fun getListsNum(): ArrayList<String> {
        var numlists = ArrayList<String>()
        numlists.add("1")
        numlists.add("2")
        numlists.add("3")
        numlists.add("4")
        numlists.add("5")
        numlists.add("6")
        return numlists;
    }
}
